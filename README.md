# Birdhouse API
Birdhouse API is a simple Express API written in TypeScript that provides a list of the 10 most common birds in Denmark, including their picture, their name in multiple languages, and the ability to play their bird call

## The Birdhouse Project
While creating a playroom for my kids, I wanted to have two birdhouses on the wall for decoration - however, they had to be interactive in some way.

My wife 3D printed two [birdhouses from Thingiverse](https://www.thingiverse.com/thing:2913735) and my mission was then to fit some eletronics into it and make it play bird tweets from the 10 most common birds found in Denmark.

I had some [Raspberry Pi Zero's](https://www.raspberrypi.com/products/raspberry-pi-zero/) laying around and found some cheap [Mini External USB Sereo Speakers](https://thepihut.com/products/mini-external-usb-stereo-speaker) that would fit inside the birdhouses together with the Raspberry Pi Zero.

<div align="center">
  <img src="documentation/birdhouse_unpainted.jpg" height=200>
  &nbsp;
  <img src="documentation/birdhouse_insides.jpg" height=200>
</div>

The next step was to create a simple and light-weight API that would play audio through the speakers - and hence, the Birdhouse API was born. The birdhouses were painted and hung on the wall above a painted tree.

Power is supplied by drilling through the wall behind the birdhouses and drawn from a utility room on the opposite side.

<div align="center">
  <img src="documentation/birdhouse_installed.jpg" height=200>
  &nbsp;
  <img src="documentation/birdhouse_painted.jpg" height=200>
</div>

Project complete! Now all that is left to do is hook it up to the playroom's interface.

At the time of writing this, the code for the playroom has not been made public, but please check out my repos; I might just have forgotten to update this README :)

<hr>

## Usage

### Prerequisites
Install all the dependencies using:

```bash
$ npm install
```

### Development
In development mode, the project runs using `nodemon` and `ts-node`. The API will restart on code changes.

```bash
$ npm run start
```

### Production
To avoid having to build some form of build pipeline and move assets around, I use `ts-node` with its `--transpileOnly` flag. This uses TypeScript's transpileModule only and does not typecheck. This should speed things up and keep the memory footprint lower.

Using this approach was suggested by the team behind ts-node.

```bash
$ npm run production
```

### Linting
Use `eslint` to lint the code:

```bash
$ npm run lint
```

## Configuration

There are several configuration options available through environment variables:

### `BIRDHOUSE_ASSETS`
This gives you the option to put the assets folder in another location.

In this folder the API expects a birds.json that includes names, audio and so on. Look at `src/assets` to see how it is configured.

### `BIRDHOUSE_API_PORT`
In case you want to run the API on a different port, set the port number using this environment variable.

The API will use the new port and you will also see it reflected in the output when the API starts up.

## API Documentation

Lists all available birds:

```javascript
GET /birds
```

Plays the provided bird ID. If an unknown or invalid ID is provided, the API responds with a 404:

```javascript
PUT /birds/:id
```

Shows the bird that is currently playing. Responds with a 204 if nothing is currently playing:

```javascript
GET /birds/playing
```
