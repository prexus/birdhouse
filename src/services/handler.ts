import { Request, Response } from 'express';

import { birdService } from './bird';

const service = {
    birds,
    play,
    playing
}

function birds(_req: Request, res: Response) {
    const birds = birdService.getBirds();
    const birdDtos = birds.map(bird => bird.toDto());

    res.send(birdDtos);
}

function play(req: Request, res: Response) {
    const birdId = parseInt(req.params.id);
    const playing = birdService.play(birdId);
    const httpCode = playing ? 204 : 404;

    res.status(httpCode).send();
}

function playing(_req: Request, res: Response) {
    const bird = birdService.getPlayingBird();
    const birdDto = bird?.toDto();

    if (birdDto) {
        res.send(birdDto);
    } else {
        res.status(204).send();
    }
}

export const handlerService = service;
