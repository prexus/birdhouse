import express from 'express';

import { handlerService } from './handler';
import { settingsService } from './settings';

const service = {
    init
}

function init() {
    const app = express()

    app.use('/images', express.static('assets/images'));

    app.get('/birds', handlerService.birds);
    app.put('/birds/:id', handlerService.play);
    app.get('/birds/playing', handlerService.playing);

    const port = settingsService.getWebserverPort();

    app.listen(port, () => {
        console.log();
        console.log(`Birdhouse API is available on port ${port}`);
    });
}

export const webserverService = service;
