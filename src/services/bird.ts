import data from '../assets/birds.json';
import { Bird } from '../classes/bird';

const birds: Bird[] = [];

const service = {
    init,
    play,
    getBirds,
    getPlayingBird
}

function init() {
    for (const entry of data) {
        console.log(`Loading ${entry.name.english}`);

        birds.push(new Bird(entry));
    }
}

function play(birdId: number) {
    const bird = getBird(birdId);

    if (bird) {
        stop();

        bird.playAudio();
    }

    return !!bird;
}

function stop() {
    const bird = getPlayingBird();

    if (bird) {
        bird.stopAudio();
    }
}

function getBird(birdId: number) {
    return birds.find(bird => bird.getId() === birdId)
}

function getBirds() {
    return birds;
}

function getPlayingBird() {
    return birds.find(bird => bird.isPlaying());
}

export const birdService = service;
