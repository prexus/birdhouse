const service = {
    getAssetsFolder,
    getWebserverPort
}

function getAssetsFolder() {
    const assetsEnv = process.env.BIRDHOUSE_ASSETS;
    const assets = assetsEnv ?? 'src/assets';

    return assets;
}

function getWebserverPort() {
    const portEnv = process.env.BIRDHOUSE_API_PORT;
    const port = portEnv ? Number(portEnv) : 3000;

    return port;
}

export const settingsService = service;
