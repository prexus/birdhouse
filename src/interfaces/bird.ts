export interface IBird {
    id: number,
    name: {
        danish: string
        english: string,
        latin: string,
        arabic: string,
        arabicPhonetic: string
    }
    image: string,
    imageVertical: string,
    audioFile: string
}
