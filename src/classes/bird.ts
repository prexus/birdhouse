import { IBird } from '../interfaces/bird';
import { Audio } from '../classes/audio';
import { settingsService } from '../services/settings';

export class Bird {
    audio: Audio;

    constructor(private readonly data: Readonly<IBird>) {
        const assetsFolder = settingsService.getAssetsFolder();
        const audioFile = `${assetsFolder}/${data.audioFile}`;

        this.audio = new Audio(audioFile);
    }

    getId() {
        return this.data.id;
    }

    playAudio() {
        this.audio.play();
    }

    stopAudio() {
        this.audio.stop();
    }

    isPlaying() {
        return this.audio.playing;
    }

    toDto() {
        return {
            id: this.data.id,
            name: this.data.name,
            image: this.data.image,
            imageVertical: this.data.imageVertical,
            playing: this.isPlaying()
        }
    }
}
