import { ChildProcess, spawn } from 'child_process';

export class Audio {
    playing = false;

    private process: ChildProcess;

    constructor(private readonly file: string) {}

    play() {
        if (this.playing) {
            this.stop();
        }

        this.playing = true;

        // Note to self:
        // This works as long as the default audio device is
        // used. Add extra params ('-a', 'plughw:1,0') in case
        // a non-default device is used for playing the audio.
        this.process = spawn('mpg123', [ this.file ]);

        this.process.on('exit', () => {
            this.playing = false;
        });
    }

    stop() {
        // Remove the exit event listener to make sure
        // this.playing is not overwritten if a newer
        // audio track is playing.
        this.process.removeAllListeners('exit');

        this.process.kill('SIGTERM');
        this.playing = false;
    }
}
