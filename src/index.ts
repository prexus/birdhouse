import { birdService } from './services/bird';
import { webserverService } from './services/webserver';

console.log('Welcome to Birdhouse API');
console.log();

birdService.init();
webserverService.init();
